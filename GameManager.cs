using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [Header("Spawn Settings")]
    public List<GameObject> fishPrefabs;
    public List<Transform> zoneASpawns;
    public List<Transform> zoneBSpawns;
    public List<Transform> zoneCSpawns;
    [Header("UI")]
    public Text targetText;
    [Header("Cutscenes")]
    public Camera cutSceneCamera;
    public PlayableDirector cutSceneDirector;
    public Vector3 cameraOffset;
    public PlayableAsset introCutScene;
    public PlayableAsset gateCutSceneA;
    public PlayableAsset gateCutSceneB;
    public PlayableAsset targetFishCutScene;
    public float targetDetectDist;
    public GameObject player;
    public GameObject targetFishPosition;
    public GameObject gateA;
    public GameObject gateB;
    public GameObject lockA;
    public GameObject lockB;
    

    public static GameManager Instance { get; private set; }
    public static IFauna Target { get; private set; }
    public bool TargetFound { get; set; } = false;
    public bool gateAPlayed { get; set; } = false;
    public bool gateBPlayed { get; set; } = false;
    #region Cutscene properties
    public bool OrbitCameraTarget { get; set; } = false;
    public bool panUpForward { get; set; } = false;
    public bool SlowFishSpeed { get; set; } = false;
    public bool RevertFishSpeed { get; set; } = false;
    public Transform CameraTarget { get; private set; }
    #endregion

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = (this);
        }
        else
        {
            enabled = false;
        }

        #region Spawn Target Fish
        GameObject targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        while(targetFish.GetComponent<IFauna>() == null)
        {
            targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        }
        fishPrefabs.Remove(targetFish);
        Transform spawn = zoneCSpawns[Random.Range(0, zoneCSpawns.Count)];
        GameObject spawnedFish = Instantiate(targetFish, spawn.transform.position, spawn.transform.rotation);
        
        if (spawnedFish.TryGetComponent(out IbSoloFish sFish) == true)
        {
            sFish.boundBox.center = spawn.position;
        }
        spawnedFish.name = targetFish.name;
        targetFishPosition = spawnedFish;
        targetText.text = targetFish.GetComponent<IbSoloFish>().nameText;
        Target = spawnedFish.GetComponent<IFauna>();
        
        zoneCSpawns.Remove(spawn);
        #endregion
        if(fishPrefabs.Count > 0 && zoneASpawns.Count > 0)
        {
            SpawnZone(zoneASpawns);
        }
        if (fishPrefabs.Count > 0 && zoneBSpawns.Count > 0)
        {
            SpawnZone(zoneBSpawns);
        }
        if (fishPrefabs.Count > 0 && zoneCSpawns.Count > 0)
        {
            SpawnZone(zoneCSpawns);
        }

       
    }

    private void Start()
    {
        
        if(cutSceneDirector != null && introCutScene != null)
        {
            StartCoroutine(PlayCutscene(introCutScene));
        }
    }

    void Update()
    {
        if(CameraTarget != null)
        {
            if(OrbitCameraTarget == true)
            {
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.up, 50 * Time.deltaTime);
                cutSceneCamera.transform.LookAt(CameraTarget);
            }
           
        }
        if(CameraTarget != null)
        {
            if(panUpForward == true)
            {
                
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.right, -25 * Time.deltaTime);
                Debug.Log("Now Panning" + cutSceneCamera.transform.position);
            }
           
        }

        //if player is within X units of the target fish
        if (targetFishPosition != null)
        {
            if (Vector3.Distance(player.transform.position, targetFishPosition.transform.position) < targetDetectDist && TargetFound != true)
            {
                Vector3 newOffset = new Vector3(15, 3, -5);
                cameraOffset = newOffset;
                CameraTarget = targetFishPosition.transform;
                PositionCinematicCamera(CameraTarget);
                StartCoroutine(PlayCutscene(targetFishCutScene));
                TargetFound = true;
                
            }
        }

        if (Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 newOffset = new Vector3(15, 3, -5);
            cameraOffset = newOffset;
            CameraTarget = targetFishPosition.transform;
            PositionCinematicCamera(CameraTarget);
            StartCoroutine(PlayCutscene(targetFishCutScene));
        }     

    }

    void SpawnZone(List<Transform> spawnList)
    {
        int zoneCount = spawnList.Count;
        for(int i = 0; i < zoneCount; i++)
        {
            int f = Random.Range(0, fishPrefabs.Count);
            Transform spawn = spawnList[Random.Range(0, spawnList.Count)];
            GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation);
            if(fish.TryGetComponent(out IbSoloFish sFish) == true)
            {
                sFish.boundBox.center = spawn.position;
            }
            fish.name = fishPrefabs[f].name;
            fishPrefabs.Remove(fishPrefabs[f]);
            spawnList.Remove(spawn);
        }
    }

    public void SlowTargetFish()
    {
        targetFishPosition.GetComponent<IbSoloFish>().speed /= 10;
        Debug.Log("Fish slowed down.");
    }

    public void RevertTargetFish()
    {
        targetFishPosition.GetComponent<IbSoloFish>().speed *= 10;
        Debug.Log("Fish speed returned to normal.");
    }

    public void TogglePlayer(bool toggle)
    {
        CharacterMove.Instance.enabled = toggle;
        MouseLook.Instance.gameObject.SetActive(toggle);
        Interaction.Instance.enabled = toggle;
        ActionMenu.Instance.enabled = toggle;
    }

    public void PositionCinematicCamera(Transform target)
    {
        CameraTarget = target;
        if(target != null)
        {
            cutSceneCamera.transform.position = target.transform.position + cameraOffset;
        }
    }

    public void SetCameraTarget(Transform target)
    {
        CameraTarget = target;
    }

    public IEnumerator PlayCutscene(PlayableAsset cutScene)
    {
        TogglePlayer(false);
        cutSceneCamera.gameObject.SetActive(true);
        cutSceneDirector.playableAsset = cutScene;
        cutSceneDirector.Play();
        while(cutSceneDirector.time != cutSceneDirector.playableAsset.duration)
        {
            yield return new WaitForEndOfFrame();
        }
        cutSceneDirector.Stop();
        cutSceneCamera.gameObject.SetActive(false);
        TogglePlayer(true);
    }


}
