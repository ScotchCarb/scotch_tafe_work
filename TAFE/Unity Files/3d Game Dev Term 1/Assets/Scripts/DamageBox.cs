using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBox : MonoBehaviour
{
    public float damage; //stores the amount of damage that this will apply to character

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out CharacterHealth health) == true)
        {
            health.OnDamage(damage);
        }
    }
}
