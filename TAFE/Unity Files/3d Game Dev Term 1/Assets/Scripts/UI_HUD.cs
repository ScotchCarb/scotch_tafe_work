using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HUD : MonoBehaviour
{
    public Image healthBarFG;

    public static UI_HUD Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public void UpdateHealth(float currentHealth, float MaxHealth)
    {
        healthBarFG.fillAmount = currentHealth / MaxHealth;

    }

    public void DisableHealthbar()
    {
        healthBarFG.transform.parent.gameObject.SetActive(false);
    }
}
