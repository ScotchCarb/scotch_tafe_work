using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FiniteStateMachine;

public class Notlek : MonoBehaviour
{
    public float viewRadius = 5.0f;
    public NotlekIdle idle;
    public NotlekChase chase;

    public StateMachine StateMachine { get; private set; } = new StateMachine();
    public NavMeshAgent NotlekAgent { get; private set; }
    public Transform Target { get; private set; }

    private void Awake()
    {
        NotlekAgent = GetComponent<NavMeshAgent>();
        idle = new NotlekIdle(this);
        chase = new NotlekChase(this, chase);
    }
    void Start()
    {
        StateMachine.SetState(idle);
    }

   
    void Update()
    {
        //check if player is nearby
        if(Random.Range(0, 10) < 1)
        {
            Target = null;
            //if yes: chase player
            foreach(Collider col in Physics.OverlapSphere(transform.position, viewRadius))
            {
                if(col.tag == "Player")
                {
                    Target = col.transform;
                    break;
                }
            }
        }

        StateMachine.OnUpdate();
    }

    private void OnDrawGizmos()
    {
        if(Target == null)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawWireSphere(transform.position, viewRadius);
    }
}


public abstract class NotlekState : IState
{
    public enum EStateID { idle, chase }
    public Notlek Instance { get; private set; }
    public EStateID ID { get; protected set; }

    public NotlekState(Notlek instance)
    {
        Instance = instance;
    }

    public virtual void OnEnter()
    {        
    }

    public virtual void OnExit()
    {        
    }

    public virtual void OnUpdate()
    {        
    }
}

public class NotlekIdle : NotlekState
{
    public NotlekIdle(Notlek instance) : base(instance)
    {
        ID = EStateID.idle;
    }

    public override void OnEnter()
    {
        Instance.NotlekAgent.isStopped = true;
    }

    public override void OnUpdate()
    {
        if(Instance.Target != null)
        {
            Instance.StateMachine.SetState(Instance.chase);
        }
    }

    public override void OnExit()
    {
        Instance.NotlekAgent.isStopped = false;
    }
}
[System.Serializable]
public class NotlekChase : NotlekState
{
    public float speedMultiplier = 2;

    private float originalSpeed = 0;
    public NotlekChase(Notlek instance, NotlekChase chase) : base(instance)
    {
        ID = EStateID.chase;
        speedMultiplier = chase.speedMultiplier;
    }

    public override void OnEnter()
    {
        originalSpeed = Instance.NotlekAgent.speed;
        Instance.NotlekAgent.speed = originalSpeed * speedMultiplier;
        Instance.NotlekAgent.isStopped = false;
    }

    public override void OnUpdate()
    {
        if(Instance.Target != null && Vector3.Distance(Instance.transform.position, Instance.Target.position) < Instance.viewRadius)
        {
            Instance.NotlekAgent.SetDestination(Instance.Target.position);
        }
        else
        {
            Instance.StateMachine.SetState(Instance.idle);
        }
    }

    public override void OnExit()
    {
        Instance.NotlekAgent.speed = originalSpeed;
    }
}