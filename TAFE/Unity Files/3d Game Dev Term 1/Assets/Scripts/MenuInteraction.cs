using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInteraction : MonoBehaviour
{
    public float distance = 2f;

    public static MenuInteraction Instance { get; private set; }


    private void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        //if the interaction menu is not active
        if (InteractionMenu.Instance.gameObject.activeSelf == false)
        {
            //check if the interaction input is pressed and no menus are open
            if (Input.GetButtonDown("Interact") == true) //and no menus open
            {
                //check if raycast hits a collider
                if (Physics.SphereCast(transform.position, 1, transform.forward, out RaycastHit hit, distance) == true)
                {
                    //check if collider has interaction component
                    if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
                    {
                        //activate the interaction
                        InteractionMenu.Instance.Activate(interaction);
                        //draw green debug line
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.3f);

                    }
                    else
                    {
                        //else draw yellow debug lin
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.yellow, 0.3f);
                    }
                }

                else
                {
                    //else draw red line
                    Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.3f);
                }
            }
            else
            {

            }
        }
        //if interact button is released
        if (Input.GetButtonUp("Interact") == true)
        {
            InteractionMenu.Instance.Deactivate();
        }
        //deactivate interaction menu
    }
}

public interface IMenuInteraction
{
    void inspect();
}

public interface IFauna : IMenuInteraction
{
    //nickelback moment
    void Photograph(string folderPath = null);
}

public interface IKey : IMenuInteraction
{
    void Pickup();
}

public interface IKeyLock : IMenuInteraction
{
    bool Unlock();
}
