using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInteraction : MonoBehaviour, IInteraction
{
    public void Activate()
    {
        Debug.Log($"Activated {gameObject.name}");
    }
}
