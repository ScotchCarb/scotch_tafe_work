using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardShuffler : MonoBehaviour
{
    
    public GameObject[] decklist; //the deck being shuffled
    private GameObject tempGO; //temporary game object used in the shuffling function

    private void Start()
    {
        DeckShuffle(); //calls the shuffle function when the script starts.
    }

    public void DeckShuffle()
    {
        for (int i =0; i < decklist.Length - 1; i++) //loops through the deck from start to finish
        {
            int rnd = Random.Range(i, decklist.Length); //chooses a random index from the array. with each iteration only the remaining unshuffled cards are used.
            tempGO = decklist[rnd]; //the randomly selected card is placed into the temporary game object
            decklist[rnd] = decklist[i]; //the randomly selected card is replaced with the current value for i
            decklist[i] = tempGO; //i is then replaced with the randomly selected card
        }
    }
}
