using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMoveController : MonoBehaviour
{
    public float speed = 5; // this will control how quickly the player can move through the world

    //motion variables
    private Vector3 input; //this will track the inputs that the player is entering as numerical values
    private Vector3 motion; // this will track the combined direction the character will need to move ona frame by frame basis

    //reference variables
    private CharacterController controller; //this will store a reference to our character controller attached to our player object

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        //reset value of motion vector to zero so it can receive new values for the current frame
        motion = Vector3.zero; //this sets all values to zero
        //detect inputs being pressed during this frame
        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Depth"), Input.GetAxis("Vertical"));
        //calculate motion
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;
        motion += Vector3.up * input.y;
        controller.Move(motion * speed * Time.deltaTime);

    }
}
