using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{

    public float maxHealth = 100;

    private float currenHealth = 0;
    
    void Start()
    {
        currenHealth = maxHealth;
        UI_HUD.Instance.UpdateHealth(currenHealth, maxHealth);
    }

    public bool OnDamage(float amount)
    {
        if(currenHealth > 0)
        {
            currenHealth -= amount;
            if(currenHealth <= 0)
            {
                currenHealth = 0;
                OnDeath();
            }
            UI_HUD.Instance.UpdateHealth(currenHealth, maxHealth);
            return true;
        }
        return false;
    }

    public void OnDeath()
    {
        UI_HUD.Instance.DisableHealthbar();
        Debug.Log(gameObject.name + " has died!");
    }
}
