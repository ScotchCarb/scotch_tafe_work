namespace FiniteStateMachine
{
    public interface IState
    {
        void OnEnter();//called on the frame that the state is entered
        void OnUpdate();//called on every frame while the state is active
        void OnExit();//called on the frame when the state ends
        }
    public class StateMachine
    {
        //variable to keep track of current state
        public IState CurrentState { get; private set; }

        public StateMachine()
        {
        }

        public StateMachine(IState initState)
        {
            //set state to initial state
            SetState(initState);
        }
        public void OnUpdate()
        {
            if (CurrentState != null)
            {
                CurrentState.OnUpdate();
            }
        }

        public void SetState(IState newState)
        {
            if (CurrentState != null)
            {
                CurrentState.OnExit();
            }
            CurrentState = newState;
            CurrentState.OnEnter();
        }
        public StateType GetCurrentStateAsType<StateType>()
where StateType : class, IState
        {
            return CurrentState as StateType;
        }
    }
}