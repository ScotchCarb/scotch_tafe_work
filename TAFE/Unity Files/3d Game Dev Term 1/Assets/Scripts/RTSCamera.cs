using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float panSpeed = 20f; //a value which controls how fast the camera will pan, can be adjusted in the Unity inspector.
    public float edgePanDistance = 10f; //controls how close to the edge of the screen the mouse cursor can get before the camera begins panning
    public Vector2 mapLimit; //used to control how far on the X & Z axis the camera can move (can be adjusted to match limits of maps)

    public float scrollSpeed = 20f; //modifier for controlling how fast zooming in and out
    public float minY = 20f; //lower limit for zooming in
    public float maxY = 120f; //upper limit for Zooming out

    public float rotateSpeed = 20f; //modifier for camera rotation speed




    void Update()
    {
        Vector3 pos = transform.position; //variable used within update to allow simutaneous inputs to register on the same frame

        //adjusting the relevant vector  'pos' variable when key is pressed or the mouse cursor is moved to the edge of the screen
        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - edgePanDistance)
        {
            pos.z += panSpeed * Time.deltaTime;
        }        
        if (Input.GetKey("s") || Input.mousePosition.y <= edgePanDistance)
        {
            pos.z -= panSpeed * Time.deltaTime;
        }       
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - edgePanDistance)
        {
            pos.x += panSpeed * Time.deltaTime;
        }       
        if (Input.GetKey("a") || Input.mousePosition.x <= edgePanDistance)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        //adjusting the rotation of the camera when the e or q key are pressed.
        if (Input.GetKey("e"))
        {
            transform.rotation = Quaternion.AngleAxis(rotateSpeed * Time.deltaTime, Vector3.right);
        }
        if (Input.GetKey("q"))
        {
            transform.rotation = Quaternion.AngleAxis(rotateSpeed * Time.deltaTime, Vector3.left);
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel"); //take input from mousewheel scrolling up and down
        pos.y -= scroll * scrollSpeed * 100 * Time.deltaTime; //apply the input from the mouse wheel to the camera's y position.

        pos.x = Mathf.Clamp(pos.x, -mapLimit.x, mapLimit.x); //stops the player from panning too far on the x
        pos.z = Mathf.Clamp(pos.z, -mapLimit.y, mapLimit.y); //stop the player from panning too far on the z
        pos.y = Mathf.Clamp(pos.y, minY, maxY); //stop the player from zooming in or out too far
        

        transform.position = pos; //apply all transformations to the camera object.
       
        
    }

    void CardShuffler()
    {

    }
}
