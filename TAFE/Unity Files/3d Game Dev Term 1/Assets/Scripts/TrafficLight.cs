using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class TrafficLight : MonoBehaviour
{
    [Range(0, 10)]
    public int waitingCars = 0;
    public OrangeLight orangeLight;

    public MeshRenderer Renderer { get; private set; }
    public StateMachine StateMachine { get; private set; }

    private void Awake()
    {
        Renderer = GetComponent<MeshRenderer>();
        StateMachine = new StateMachine();
        orangeLight = new OrangeLight(this);
    }

    void Start()
    {
        StateMachine.SetState(new RedLight(this));
    }

    void Update()
    {
        if(waitingCars == 0)
        {
            if(StateMachine.GetCurrentStateAsType<TrafficLightState>().ID == TrafficLightID.green)
            {
                StateMachine.SetState(orangeLight);
            }
        }
        else if (waitingCars >= 5)
        {
            if(StateMachine.GetCurrentStateAsType<TrafficLightState>().ID != TrafficLightID.green)
            {
                StateMachine.SetState(new GreenLight(this));
            }
        }
        StateMachine.OnUpdate();
    }
    
    public enum TrafficLightID { green = 0, orange = 1, red = 2 }

    public abstract class TrafficLightState : IState
    {
        public TrafficLightID ID { get; protected set; }
        protected TrafficLight instance;

        public TrafficLightState(TrafficLight _instance)
        {
            instance = _instance;
        }

        public virtual void OnEnter()
        {            
        }

        public virtual void OnUpdate()
        {            
        }

        public virtual void OnExit()
        {            
        }
    }

    public class GreenLight : TrafficLightState
    {
        public GreenLight(TrafficLight _instance) : base(_instance)
        {
            ID = TrafficLightID.green;
        }

        public override void OnEnter()
        {
            instance.Renderer.material.color = Color.green;
            Debug.Log("The Light Has Turned Green");
        }

        public override void OnUpdate()
        {
            Debug.Log("The Light Is Currently Green");
        }

        public override void OnExit()
        {
            Debug.Log("The Light Is No Longer Green");
        }
    }
    [System.Serializable]
    public class OrangeLight : TrafficLightState
    {
        public float time = 3;

        private float timer = 0;
        public OrangeLight(TrafficLight _instance) : base(_instance)
        {
            ID = TrafficLightID.orange;
        }

        public override void OnEnter()
        {
            timer = 0;
            instance.Renderer.material.color = Color.yellow;
            Debug.Log("The Light Has Turned Orange");
        }

        public override void OnUpdate()
        {
            timer += Time.deltaTime;
            if(timer > time)
            {
                instance.StateMachine.SetState(new RedLight(instance));
            }
            Debug.Log("The Light Is Currently Orange");
        }

        public override void OnExit()
        {
            Debug.Log("The Light Is No Longer Orange");
        }
    }

    public class RedLight : TrafficLightState
    {
        public RedLight(TrafficLight _instance) : base(_instance)
        {
            ID = TrafficLightID.red;
        }

        public override void OnEnter()
        {
            instance.Renderer.material.color = Color.red;
            Debug.Log("The Light Has Turned Red");
        }

        public override void OnUpdate()
        {
            Debug.Log("The Light Is Currently Red");
        }

        public override void OnExit()
        {
            Debug.Log("The Light Is No Longer Red");
        }
    }
}
