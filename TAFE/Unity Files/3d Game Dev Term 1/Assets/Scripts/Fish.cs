using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour, IFauna
{
    public void inspect()
    {
        Debug.Log("Today's Fish Is Trout ala Creme. Enjoy your meal.");

    }

    public void Photograph(string folderPath = null)
    {
        if (folderPath != null)
        {
            string date = System.DateTime.Now.TimeOfDay.ToString().Replace(':', '-');
            ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2);
        }
    }


}
