#define function for performing a linear search which will confirm if the user's operating system is in the list.
def os_search(Tuple, x):
    for os in range (len(Tuple)):
        if Tuple[os] == x:
            return os
    return -1

#create a Tuple data structure with the data from the table of operating systems.
op_systems = ("Windows 10", "Linux Mint", "Mac OS 11", "Android Oreo", "Android Pie", "Android 11", "iOS 14")

#prompt user for input to store as string
user_input = input('Please enter the name of the Operating System you are searching for: ')

#search string taken from user input - the input() function
#convert capitalization of user input to lower case
fixed_input = user_input.lower()
#convert (some) common mispellings of user input. God help them if they can't spell pei.
fixed_input = fixed_input.replace('wnidows', 'windows')
fixed_input = fixed_input.replace('windwos', 'windows')
fixed_input = fixed_input.replace('lniux', 'linux')
fixed_input = fixed_input.replace('andriod', 'android')
fixed_input = fixed_input.replace('adnroid', 'android')
fixed_input = fixed_input.replace('mnit', 'mint')
#convert user input of strings with words instead of numbers
fixed_input = fixed_input.replace('ten', '10')
fixed_input = fixed_input.replace('eleven', '11')
fixed_input = fixed_input.replace('fourteen', '14')
#convert user input of strings without spaces
fixed_input = fixed_input.replace('windows10', 'windows 10')
fixed_input = fixed_input.replace('linuxmint', 'linux mint')
fixed_input = fixed_input.replace('macos11', 'mac os 11')
fixed_input = fixed_input.replace('androidoreo', 'android oreo')
fixed_input = fixed_input.replace('androidpie', 'android pie')
fixed_input = fixed_input.replace('android11', 'android 11')
fixed_input = fixed_input.replace('ios14', 'ios 14')

#convert user input to capitalized versions of any valid input
fixed_input = fixed_input.replace('windows', 'Windows')
fixed_input = fixed_input.replace('linux', 'Linux')
fixed_input = fixed_input.replace('mint', 'Mint')
fixed_input = fixed_input.replace('mac', 'Mac')
fixed_input = fixed_input.replace('android', 'Android')
fixed_input = fixed_input.replace('pie', 'Pie')
fixed_input = fixed_input.replace('oreo', 'Oreo')
fixed_input = fixed_input.replace('ios', 'iOS')
fixed_input = fixed_input.replace('os', 'OS')

#inform user that the system is searching for input which may have been altered:
print('Searching for: ', fixed_input, '\n(Note: spelling or formatting errors may have been corrected.')

#method for doing a linear search of the data structure using user input string after 'normalizing'
osFound = os_search(op_systems, fixed_input)


if osFound != -1:
    print('The list of operating systems is stored in a: ', type(op_systems))
    print(fixed_input, " is in the list at position: ", osFound, "\nGoodbye.") #print the name of the operating system and its position to console, using type() Python library function to output to the console the type of data structure (Tuple)    
else:
    print("That operating system is not on the list. Goodbye.")





