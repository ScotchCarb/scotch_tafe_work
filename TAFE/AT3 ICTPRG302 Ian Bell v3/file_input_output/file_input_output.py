#concatenate the two strings and store them as a variable.
output_string = "Python has been an important part of Google since the beginning, and remains so as the system grows and evolves.\n" + "“Today dozens of Google engineers use Python, and we're looking for more people with skills in this language.” said Peter Norvig, director of search quality at Google, Inc."

#write contents of output_string into "Python.txt"
file = open("Python.txt", "w") #this will open a .txt file in the same directory as the solution - if the file doesn't exit, it will create it.
file.write(output_string) #writes the stored contents of output_string to Python.txt.
file.close() #closes the file once finished writing to it.

#read contents of "Python.txt" into string variable and store it as input_string
from pathlib import Path #imports the main class 'Path', a Python module for object-oriented file system paths.
input_string = Path('Python.txt').read_text() #opens the text file, reads the text from it and assigns it to the variable then closes it in the same line.

#print out contents of input_string to console
print(input_string)