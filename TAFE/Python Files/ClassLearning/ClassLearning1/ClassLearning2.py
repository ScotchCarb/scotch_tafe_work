
number = 1
print(number)

#this will 'cast' the variable - not changing it, but displaying it as another type
print (float(number))

#this will convert the variable to a new type
number = float(number)
print (number)

#this changes it back
number = int(number)
print(number)

#this converts the variable to float through addition
number = number + 1.0
print (number)



result = sum(7, 3)
#if/else statement looks like this
if(result != 10):
    print ("Foo")
elif(result == 10):
    print ("Bar")

#message = input("Please enter some input: ")
#print (message)



"""conversion to string:
number="Number: " + str(number)
print(number)"""

"""cannot convert back to int
number = int(number)
print(number)
^^ that will crash the program"""

#Method to add 2 numbers together
def sum (a, b,):
    return a+b

#Method to subtract 1 number from another number
def subtract (a, b):
    return (a-b)

#Method to divide 2 numbers (number 1 by number 2)
def divide (a, b):
    return (a/b)

#Method to multiply 2 numbers (number 1 by number 2)
def multiply (a, b):
    return (a*b)


#to call those methods:
print(sum(2, 3))
print(subtract(2, 3))
print(divide(2, 3))
print(sum(2, 3))