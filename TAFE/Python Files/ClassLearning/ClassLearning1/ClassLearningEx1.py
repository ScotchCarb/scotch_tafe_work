#Exercise to work out the length of a string

message = input("Please enter some input: ")
message_len = len(message) #establishes the length of the variable 'message'

#checks if 'message' is under a certain length, and prints the message if it is under that length.
if(message_len > 0):
    if (message_len >= 30):
        print("Message cannot be longer than 29 characters")
    elif(message_len < 30):
        if(str.upper("Yes") in str.upper(message)):
            print("Found 'Yes' in " + message)
        else:
            print("You didn't type Yes in" + message)
        

#when we send information to a function that is a 'parameter' and is always in paranetheses
