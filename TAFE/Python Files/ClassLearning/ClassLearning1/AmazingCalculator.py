
#Method to add 2 numbers together
def sum (a, b,):
    return a+b

#Method to subtract 1 number from another number
def subtract (a, b):
    return (a-b)

#Method to divide 2 numbers (number 1 by number 2)
def divide (a, b):
    return (a/b)

#Method to multiply 2 numbers (number 1 by number 2)
def multiply (a, b):
    return (a*b)


#prompts user to enter 2 numbers
print("Please enter 2 numbers: ")
#inputs for the numbers
Num1 = input()
Num2 = input()

#converts the input into integer so that mathematical calculations can be performed on them
Num1 = int(Num1)
Num2 = int(Num2)


#prints the statement 'Your numbers...'
print("Your numbers...")
#Prints stuff then calls those methods:
print("Added together: " + str(sum(Num1, Num2))) #print will only prints strings  if you have printed a string, so you need to cast the type back into str
print("Subtracting first from second: " + str(subtract(Num1, Num2)))
print("Dividing first by second: " + str(divide(Num1, Num2)))
print("Multiplying first by second: " + str(multiply(Num1, Num2)))