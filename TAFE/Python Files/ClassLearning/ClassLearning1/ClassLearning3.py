#Creating a class & collections

#creating the class 'Weapon'
class Weapon:
    #constructor
    def __init__(self, _name):
        self.name = _name
    #variable for storing name
    name = "Default Weapon"
    #method for calling shoot
    def shoot():
        print("shoot weapon")

#creating instances of weapon object
pistol = Weapon("Pistol")
rifle = Weapon("Rifle")
sniper = Weapon("Sniper")

m_index = 2

#this is a list
weapons = [pistol, rifle, sniper]

#for loop
for weapon in weapons:
    if(weapons.index(weapon)==m_index):
        print(weapon.name)

i = 0
while(i < 1000):
    print(i)
    i += 1
    if (i == 500):
        print("Continue hit")
        continue
x = 5
y = 5

for weapon in weapons:
    if (x == 5 and y == 5):
        print("gay")

