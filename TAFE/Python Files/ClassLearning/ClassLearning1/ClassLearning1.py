"""this is how to do a multiline
comment in python"""
#this is how to do a single line comment in python

my_float = 2.0
print("My float is equal to - " + str(my_float))
my_float = "Is now a string"
print("My float is equal to - " + str(my_float))
ascii_art = """
(\,--------'()'--o
 (_    ___    /~"
  (_)_)  (_)_)"""
print(ascii_art)