using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensitivity = 2.5f;
    public float drag = 1.5f;
     
    private Transform character;
    private Vector2 mouseDir;
    private Vector2 smoothing;
    private Vector2 result;

    public static MouseLook Instance { get; set; }

    public bool LookEnabled { get; set; } = true;
    public bool CursorToggle 
    {
        set
        {
            Cursor.visible = value;
            if(value == true)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    private void Awake()
    {
        //singleton setup
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }

        character = transform.parent;//set reference to character transform
        CursorToggle = false;//turn the cursor off
    }

    void Update()
    {
        if(LookEnabled == true)
        {
            //get cursor position
            mouseDir = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            mouseDir *= sensitivity; // multiply with sensitivity
            //interpolate between smoothing vector and mouseDir over the drag ratio
            smoothing = Vector2.Lerp(smoothing, mouseDir, 1 / drag);
            result += smoothing; //apply smoothing to the result
            result.y = Mathf.Clamp(result.y, -80, 80); //clamp Y value
            transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right); //apply angle rotation for camera
            character.rotation = Quaternion.AngleAxis(result.x, character.transform.up); //apply camera rotation for parnt



        }
    }
}
