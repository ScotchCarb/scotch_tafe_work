using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2;

    public static Interaction Instance { get; private set; }

    private List<Key.KeyType> keyRing = new List<Key.KeyType>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }


    void Update()
    {
        //if interaction menu is not open
        if (InteractionMenu.Instance.gameObject.activeSelf == false)
        {
            //if action menu and map are not open then execute the following:
            if(ActionMenu.Instance.gameObject.activeSelf == false && WorldMap.Instance.legend.activeSelf == false) 
            {
                if (Input.GetButtonDown("Interaction") == true)
                {
                    //if spherecast hits something
                    if (Physics.SphereCast(transform.position, 0.5f, transform.forward, out RaycastHit hit, distance) == true)
                    {
                        //check hit for interaction type
                        if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
                        {
                            //activate interaction
                            InteractionMenu.Instance.Activate(interaction);
                            Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.5f);
                        }
                        else
                        {
                            Debug.DrawRay(transform.position, transform.forward * distance, Color.yellow, 0.5f);
                        }

                    }
                    else
                    {
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.5f);
                    }
                }
                
                


            }
            
        }
        else
        {
            if (Input.GetButtonUp("Interaction") == true)
            {
                InteractionMenu.Instance.Deactivate();
            }
        }
        //else check if interact button is released

    }

    public bool AddKey(Key.KeyType key)
    {
        if(keyRing.Contains(key) == false)
            {
            keyRing.Add(key);
            return true;
            }
        return false;
    }

    public bool HasKey(Key.KeyType key)
    {
        return keyRing.Contains(key);
    }

}

public interface IInteraction
{
    string Inspect();
}

public interface IFauna : IInteraction
{
    void Photograph(string folderPath = null);
}

public interface IKey : IInteraction
{
    void Pickup();
}

public interface IGate : IInteraction
{
    bool Unlock();
}

public interface IShip : IInteraction
{
    void Leave();
}