using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [Header("Spawn Settings")]
    public List<GameObject> fishPrefabs;
    public List<Transform> zoneASpawns;
    public List<Transform> zoneBSpawns;
    public List<Transform> zoneCSpawns;
    [Header("UI")]
    public Text targetText;
    [Header("Cutscenes")]
    public Camera cutSceneCamera;
    public PlayableDirector cutSceneDirector;
    public Vector3 cameraOffset;
    public PlayableAsset introCutScene;
    public PlayableAsset gateCutScene;
    public PlayableAsset targetFishCutScene;

    public static GameManager Instance { get; private set; }
    public static IFauna Target { get; private set; }
    public bool TargetFound { get; set; } = false;
    #region Cutscene properties
    public bool OrbitCameraTarget { get; set; } = false;
    public Transform CameraTarget { get; private set; }
    #endregion

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = (this);
        }
        else
        {
            enabled = false;
        }

        #region Spawn Target Fish
        GameObject targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        while(targetFish.GetComponent<IFauna>() == null)
        {
            targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        }
        fishPrefabs.Remove(targetFish);
        Transform spawn = zoneCSpawns[Random.Range(0, zoneCSpawns.Count)];
        GameObject spawnedFish = Instantiate(targetFish, spawn.transform.position, spawn.transform.rotation);
        if (spawnedFish.TryGetComponent(out SoloFish sFish) == true)
        {
            sFish.boundBox.center = spawn.position;
        }
        spawnedFish.name = targetFish.name;
        targetText.text = targetFish.name;
        Target = spawnedFish.GetComponent<IFauna>();
        zoneCSpawns.Remove(spawn);
        #endregion
        if(fishPrefabs.Count > 0 && zoneASpawns.Count > 0)
        {
            SpawnZone(zoneASpawns);
        }
        if (fishPrefabs.Count > 0 && zoneBSpawns.Count > 0)
        {
            SpawnZone(zoneBSpawns);
        }
        if (fishPrefabs.Count > 0 && zoneCSpawns.Count > 0)
        {
            SpawnZone(zoneCSpawns);
        }
    }

    private void Start()
    {
        if(cutSceneDirector != null && introCutScene != null)
        {
            StartCoroutine(PlayCutscene(introCutScene));
        }
    }

    void Update()
    {
        if(CameraTarget != null)
        {
            if(OrbitCameraTarget == true)
            {
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.up, 50 * Time.deltaTime);
                cutSceneCamera.transform.LookAt(CameraTarget);
            }
        }
    }

    void SpawnZone(List<Transform> spawnList)
    {
        int zoneCount = spawnList.Count;
        for(int i = 0; i < zoneCount; i++)
        {
            int f = Random.Range(0, fishPrefabs.Count);
            Transform spawn = spawnList[Random.Range(0, spawnList.Count)];
            GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation);
            if(fish.TryGetComponent(out SoloFish sFish) == true)
            {
                sFish.boundBox.center = spawn.position;
            }
            fish.name = fishPrefabs[f].name;
            fishPrefabs.Remove(fishPrefabs[f]);
            spawnList.Remove(spawn);
        }
    }

    public void TogglePlayer(bool toggle)
    {

        CharacterMove.Instance.enabled = toggle;
        MouseLook.Instance.gameObject.SetActive(toggle);
        Interaction.Instance.enabled = toggle;
        ActionMenu.Instance.enabled = toggle;
        


    }

    public void PositionCinematicCamera(Transform target)
    {
        CameraTarget = target;
        if(target != null)
        {
            cutSceneCamera.transform.position = target.position + cameraOffset;
        }
    }

    public IEnumerator PlayCutscene(PlayableAsset cutScene)
    {
        TogglePlayer(false);
        cutSceneCamera.gameObject.SetActive(true);
        cutSceneDirector.playableAsset = cutScene;
        cutSceneDirector.Play();
        while(cutSceneDirector.time != cutSceneDirector.playableAsset.duration)
        {
            yield return new WaitForEndOfFrame();
        }
        cutSceneDirector.Stop();
        cutSceneCamera.gameObject.SetActive(false);
        TogglePlayer(true);
    }
}
