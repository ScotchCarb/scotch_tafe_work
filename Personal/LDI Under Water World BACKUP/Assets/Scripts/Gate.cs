using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour, IGate
{
    [TextArea]
    public string toolTipText;
    public Key.KeyType type;

    public bool Locked { get; private set; } = true;

    void Start()
    {
        WorldMap.Instance.AddInteraction(gameObject);
    }
    public string Inspect()
    {
        return toolTipText;
    }

    public bool Unlock()
    {
        if (Locked == true)
        {
            if(Interaction.Instance.HasKey(type) == true)
            {
                Locked = false;
                Debug.Log("Cutscene " + type + " has been triggered!!");
                return true;
            }
            else
            {
                //tooltip "you are missing the key"
                Debug.Log("Key is missing");
                
            }
        }
        else
        {
            //tooltip "already unlocked"
            Debug.Log("Already unlocked!");
        }
        return false;
    }
}
