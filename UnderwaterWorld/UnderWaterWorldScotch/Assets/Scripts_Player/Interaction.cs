using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 10f; //max distance a player can interact with objects from


 
    void Update()
    {
     if(Input.GetKeyDown(KeyCode.F) == true)
        {
            if(Physics.SphereCast(transform.position, 5, transform.forward, out RaycastHit hit, distance) == true)
            {
                if(hit.collider.TryGetComponent(out IInteraction interaction) == true)
                {
                    interaction.Activate();
                    Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.5f);
                   
                }
                else
                {
                    Debug.DrawRay(transform.position, transform.forward * distance, Color.yellow, 0.5f);
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.5f);
            }
        }   
    }
}

public interface IInteraction
{
    void Activate();
}