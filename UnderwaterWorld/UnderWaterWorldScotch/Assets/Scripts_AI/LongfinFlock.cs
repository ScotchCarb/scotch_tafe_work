using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongfinFlock : MonoBehaviour
{
    //Settings for this flock:
    [Header("Spawn Settings")]
    //which prefab AI Agent is being spawned
    [SerializeField] private LongfinBehaviour longfinPrefab;
    //how many agents will spawn for this flock
    [SerializeField] private int flockSize;
    //the size of the area in which the flock will spawn
    [SerializeField] private Vector3 spawnArea;

    //settings for the speed of Agents within this flock
    [Header("Speed Settings")]
    //range for the minimum speed field and the slider within unity for setting it
    [Range(0, 10)]
    [SerializeField] private float _minSpeed;
    //variable for the minimum speed that can be adjusted by other functions without changing _minSpeed
    public float minSpeed { get { return _minSpeed; } }

    //range for the maximum speed field and the slider in unity for setting it
    [Range(0, 10)]
    [SerializeField] private float _maxSpeed;
    //variable for the maximum speed that can be adjusted by other functions without changing _minSpeed
    public float maxSpeed { get { return _maxSpeed; } }

    //settings for how close things need to be for flocking behaviour to execute
    [Header("Detection Distances")]
    [Range(0, 10)]
    [SerializeField] private float _cohesionDistance;
    public float cohesionDistance { get { return _cohesionDistance; } }

    [Range(0, 10)]
    [SerializeField] private float _avoidanceDistance;
    public float avoidanceDistance { get { return _avoidanceDistance; } }

    [Range(0, 10)]
    [SerializeField] private float _alignmentDistance;
    public float alignmentDistance { get { return _alignmentDistance; } }

    [Range(0, 100)]
    [SerializeField] private float _boundsDistance;
    public float boundsDistance { get { return _boundsDistance; } }

    [Range(0, 10)]
    [SerializeField] private float _obstacleDistance;
    public float obstacleDistance { get { return _obstacleDistance; } }

    //settings for how much each type of behaviour changes the steering of an Agent
    [Header("Behaviour Weights")]
    [Range(0, 10)]
    [SerializeField] private float _cohesionWeight;
    public float cohesionWeight { get { return _cohesionWeight; } }

    [Range(0, 10)]
    [SerializeField] private float _avoidanceWeight;
    public float avoidanceWeight { get { return _avoidanceWeight; } }

    [Range(0, 10)]
    [SerializeField] private float _alignmentWeight;
    public float alignmentWeight { get { return _alignmentWeight; } }
    [Range(0, 10)]
    [SerializeField] private float _boundsWeight;
    public float boundsWeight { get { return _boundsWeight; } }
    [Range(0, 100)]
    [SerializeField] private float _obstacleWeight;
    public float obstacleWeight { get { return _obstacleWeight; } }

    //references the LongfunBehaviour script
    public LongfinBehaviour[] allUnits { get; set; }



    void Start()
    {
        //calls the GenerateUnits method which will spawn in the flock members.
        SpawnUnits();
    }


    void Update()
    {
        //cycles through each member of the flock and calls the MoveUnit method to apply move transformations.
        for (int i = 0; i < allUnits.Length; i++)
        {
            allUnits[i].MoveUnit();
        }
    }

    //method for spawning in the flock members
    private void SpawnUnits()
    {
        //creates a new instance of LongfinBehaviour and sets the flock size
        allUnits = new LongfinBehaviour[flockSize];
        //iterate through the flock and spawn in an instance of the Longfin Batfish prefab AI Agent on each loop
        for (int i = 0; i < flockSize; i++)
        {
            var randomVector = UnityEngine.Random.insideUnitSphere;
            //chooses a random spot within the bounds of the spawn area to place a new agent each loop
            randomVector = new Vector3(randomVector.x * spawnArea.x, randomVector.y * spawnArea.y, randomVector.z * spawnArea.z);
            var spawnPosition = transform.position + randomVector;
            var rotation = Quaternion.Euler(0, UnityEngine.Random.Range(0, 360), 0);
            allUnits[i] = Instantiate(longfinPrefab, spawnPosition, rotation);
            allUnits[i].AssignFlock(this);
            allUnits[i].InitializedSpeed(UnityEngine.Random.Range(minSpeed, maxSpeed));
        }

    }
}

