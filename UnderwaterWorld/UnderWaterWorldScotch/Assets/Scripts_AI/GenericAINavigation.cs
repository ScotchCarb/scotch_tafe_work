using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FiniteStateMachine;


public class GenericAINavigation : MonoBehaviour
{
    //public variables:
    public float viewRadius = 50.0f; //defines the radius of the sphere around the AI which can be used to detect other GameObjects
    public float chillRadius = 5f; //defines the radius in which the tiger shark will stop 'chasing' it's destination and instead 'chill' with it.
    public AIIdlePlayer idlePlayer;
    public AIIdleNav idleNav;
    public AIChasePlayer chasePlayer;
    public AIChaseNav chaseNav;
    public float curiosity;

    public bool playerChill;
    public bool navChill;
    public bool curious;
    

    //public autoproperties:
    public StateMachine StateMachine { get; private set; } = new StateMachine(); // 'get; private set;' means that other game objects can get the current state of this object
    //but the current state of the object can only be set within this class.
    public NavMeshAgent Agent { get; private set; }
    public Transform playerTarget { get; private set; }
    public Transform navTarget { get; private set; }

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        idlePlayer = new AIIdlePlayer(this);
        idleNav = new AIIdleNav(this);
        chasePlayer = new AIChasePlayer(this);
        chaseNav = new AIChaseNav(this);
        curiosity = 10f;
        navTarget = transform.parent;
        navChill = false;
        playerChill = false;
        curious = true;
    }


    
    void Start()
    {
        StateMachine.SetState(idlePlayer);
    }

    
    void Update()
    {
        //detect if (an object) is closer to the AI
        if(Random.Range(0, 10) < 1)
        {
            NavChillRange();
            PlayerChillRange();
            CuriosityCheck();

            playerTarget = null;
            foreach(Collider col in Physics.OverlapSphere(transform.position, viewRadius)) //this will cycle through each collider within the viewRadius on that frame
            {
                if(col.tag == "Player") //if the tag of the current collider being iterated through is 'Player'
                {
                    playerTarget = col.transform; //...set the target to be equal to the transform of that collider (the Player)
                    break; //then break the loop.
                }
            }
        }

        StateMachine.OnUpdate();
        

    }

    private void OnDrawGizmos()
    {
        if (playerTarget == null)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawWireSphere(transform.position, viewRadius);
    }

    //method to check if we're in chill range of: navPoint
    private void NavChillRange()
    {
        if(Vector3.Distance(transform.position, navTarget.position) > chillRadius)
        {
            navChill = false;
        }
        else
        {
            navChill = true;
        }
    }

    //method to check if we're in chill range of: playerTarget
    private void PlayerChillRange()
    {
        if (Vector3.Distance(transform.position, navTarget.position) > chillRadius)
        {
            playerChill = false;
        }
        else
        {
            playerChill = true;
        }
    }

    //method to check if curiosity is over 0
    private void CuriosityCheck()
    {
        if(curiosity <= 0)
        {
            curious = false;
        }
        else
        {
            curious = true;
        }
    }


}



public abstract class AIState : IState
{
    public enum EStateID { idlePlayer, chasePlayer, idleNav, chaseNav }
    public GenericAINavigation Instance { get; private set; }
    public EStateID ID { get; protected set; }

    public AIState(GenericAINavigation instance)
    {
        Instance = instance;
    }
    public virtual void OnEnter()
    {        
    }

    public virtual void OnExit()
    {        
    }

    public virtual void OnUpdate()
    {        
    }
}

public class AIIdlePlayer : AIState
{
    public AIIdlePlayer(GenericAINavigation instance) : base(instance)
    {
        ID = EStateID.idlePlayer;
    }

    public override void OnEnter()
    {
        
    }

    public override void OnUpdate()
    {
        if(Instance.playerTarget != null)
        {
            Instance.StateMachine.SetState(Instance.chasePlayer);
        }
    }

    public override void OnExit()
    {
       
    }
}

public class AIChasePlayer : AIState
{
    public float speedMultiplier = 2;

    private float originalSpeed = 0;

    public AIChasePlayer(GenericAINavigation instance) : base(instance)
    {
        ID = EStateID.chasePlayer;
    }

    public override void OnEnter()
    {
        originalSpeed = Instance.Agent.speed;
        Instance.Agent.speed = originalSpeed * speedMultiplier;
       
    }

    public override void OnUpdate()
    {
       

        if (Instance.playerTarget != null && Vector3.Distance(Instance.transform.position, Instance.playerTarget.position) < Instance.viewRadius)
        {            

            if (Instance.playerTarget.position.y - 5 > Instance.transform.position.y)
            {
                Instance.Agent.baseOffset += Instance.Agent.speed * speedMultiplier * Time.deltaTime;
                Instance.Agent.SetDestination(Instance.playerTarget.position);
                
                Vector3 vertLook = Instance.playerTarget.position;                
                Instance.transform.LookAt(vertLook);
                
                
            }
            else if (Instance.playerTarget.position.y + 5 < Instance.transform.position.y)
            {
                Instance.Agent.baseOffset -= Instance.Agent.speed * speedMultiplier * Time.deltaTime;
                Instance.Agent.SetDestination(Instance.playerTarget.position);
               
                Vector3 vertLook = Instance.playerTarget.position;                
                Instance.transform.LookAt(vertLook);

            }
            else
            {
                Instance.Agent.SetDestination(Instance.playerTarget.position);
                
                Vector3 vertLook = Instance.playerTarget.position;                
                Instance.transform.LookAt(vertLook);

            }
            
        }
        else
        {
            Instance.StateMachine.SetState(Instance.idlePlayer);
        }
    }

    public override void OnExit()
    {
        Instance.Agent.speed = originalSpeed;
    }

}

public class AIIdleNav : AIState
{
    public float speedMultiplier = 0.5f;

    private float originalSpeed = 0f;
    public AIIdleNav(GenericAINavigation instance) : base(instance)
    {
        ID = EStateID.idleNav;
    }

    public override void OnEnter()
    {
        Instance.Agent.isStopped = true;
    }

    public override void OnUpdate()
    {
        if(Instance.playerTarget != null && Vector3.Distance(Instance.transform.position, Instance.playerTarget.position) < Instance.viewRadius && Instance.curious == true) 
        {
            Instance.StateMachine.SetState(Instance.chasePlayer);
        }
        else if (Instance.navChill == false)
        {
            Instance.StateMachine.SetState(Instance.chaseNav);
        }
        else
        {
            //method for moving the shark randomly within the chill radius of the navPoint          
        }
    }

    public override void OnExit()
    {
        Instance.Agent.isStopped = false;
    }
}

public class AIChaseNav : AIState
{
    public float speedMultiplier = 2;

    private float originalSpeed = 0;

    public AIChaseNav(GenericAINavigation instance) : base(instance)
    {
        ID = EStateID.chaseNav;
    }

    public override void OnEnter()
    {
        originalSpeed = Instance.Agent.speed;
        Instance.Agent.speed = originalSpeed * speedMultiplier;
        
    }

    public override void OnUpdate()
    {
        //check if we have a player target, if the player target is within view, if curiosity is greater than 0)
        if (Instance.playerTarget != null && Vector3.Distance(Instance.transform.position, Instance.playerTarget.position) < Instance.viewRadius && Instance.curious == true)
        {
            Instance.StateMachine.SetState(Instance.chasePlayer);
        }
        //check if the shark is within the chill range of the navPoint
        else if (Instance.navChill == true)
        {
            Instance.StateMachine.SetState(Instance.idleNav);
        }
        //otherwise, move towards the navpoint.
        else
        {
            if (Instance.navTarget.position.y > Instance.transform.position.y)
            {
                Instance.Agent.baseOffset += Instance.Agent.speed * speedMultiplier * Time.deltaTime;
                Instance.Agent.SetDestination(Instance.navTarget.position);

                Vector3 vertLook = Instance.navTarget.position;
                Instance.transform.LookAt(vertLook);


            }
            else if (Instance.navTarget.position.y < Instance.transform.position.y)
            {
                Instance.Agent.baseOffset -= Instance.Agent.speed * speedMultiplier * Time.deltaTime;
                Instance.Agent.SetDestination(Instance.navTarget.position);

                Vector3 vertLook = Instance.navTarget.position;
                Instance.transform.LookAt(vertLook);

            }
            else
            {
                Instance.Agent.SetDestination(Instance.navTarget.position);

                Vector3 vertLook = Instance.navTarget.position;
                Instance.transform.LookAt(vertLook);

            }
        }
    }

    public override void OnExit()
    {
        Instance.Agent.speed = originalSpeed;
    }




}


