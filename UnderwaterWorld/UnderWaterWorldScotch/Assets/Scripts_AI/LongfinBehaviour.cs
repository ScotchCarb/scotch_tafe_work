using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LongfinBehaviour : MonoBehaviour
{
    [SerializeField] private float FOVAngle; //slider for setting the field of view of each AI Agent
    [SerializeField] private float smoothDamp; //slider for changing the smooth dampening for the Agent's steering
    [SerializeField] private LayerMask obstacleMask; //
    [SerializeField] private Vector3[] directionsObstacleDodgeCheck; //slider for adjusting the direction that the obstacle avoiding raycasts are sent

    private List<LongfinBehaviour> flockNeighbours = new List<LongfinBehaviour>();
    private List<LongfinBehaviour> avoidFlockNeighbours = new List<LongfinBehaviour>();
    private List<LongfinBehaviour> alignFlockNeighbours = new List<LongfinBehaviour>();
    private LongfinFlock assignedFlock;
    private Vector3 currentVelocity;
    private Vector3 currentObstacleAvoidanceVector;
    private float speed;

    public Transform myTransform { get; set; }

    private void Awake()
    {
        myTransform = transform;
    }

    public void AssignFlock(LongfinFlock flock)
    {
        assignedFlock = flock;
    }

    public void InitializedSpeed (float speed)
    {
        this.speed = speed;
    }

    public void MoveUnit()
    {
        FindNeighbours();
        CalculateSpeed();
        var cohesionVector = CalculateCohesionVector() * assignedFlock.cohesionWeight;
        var avoidanceVector = CalculateAvoidanceVector() * assignedFlock.avoidanceWeight;
        var alignmentVector = CalculateAlignmentVector() * assignedFlock.alignmentWeight;
        var boundsVector = CalculateBoundsVector() * assignedFlock.boundsWeight;
        var obstacleVector = ObstacleAvoidVector() * assignedFlock.obstacleWeight;

        var moveVector = cohesionVector + alignmentVector + avoidanceVector + boundsVector + obstacleVector;
        moveVector = Vector3.SmoothDamp(myTransform.forward, moveVector, ref currentVelocity, smoothDamp);
        moveVector = moveVector.normalized * speed;
        if (moveVector == Vector3.zero)
            moveVector = transform.forward;

        myTransform.forward = moveVector;
        myTransform.position += moveVector * Time.deltaTime;
    }

    private void FindNeighbours()
    {
        flockNeighbours.Clear();
        avoidFlockNeighbours.Clear();
        alignFlockNeighbours.Clear();

        var allUnits = assignedFlock.allUnits;
        for (int i = 0; i < allUnits.Length; i++)
        {
            var currentUnit = allUnits[i];
            if (currentUnit != this)
            {
                float currentNeighbourDistanceSqr = Vector3.SqrMagnitude(currentUnit.myTransform.position - transform.position);
                if (currentNeighbourDistanceSqr <= assignedFlock.cohesionDistance * assignedFlock.cohesionDistance)
                {
                    flockNeighbours.Add(currentUnit);
                }
                if (currentNeighbourDistanceSqr <= assignedFlock.avoidanceDistance * assignedFlock.avoidanceDistance)
                {
                    avoidFlockNeighbours.Add(currentUnit);
                }
                if (currentNeighbourDistanceSqr <= assignedFlock.alignmentDistance * assignedFlock.alignmentDistance)
                {
                    alignFlockNeighbours.Add(currentUnit);
                }
            }
        }


    }

    private void CalculateSpeed()
    {
        if (flockNeighbours.Count == 0)
            return;
        speed = 0;
        for (int i = 0; i < flockNeighbours.Count; i++)
        {
            speed += flockNeighbours[i].speed;
        }

        speed /= flockNeighbours.Count;
        speed = Mathf.Clamp(speed, assignedFlock.minSpeed, assignedFlock.maxSpeed);
    }

    private Vector3 CalculateCohesionVector()
    {
        var cohesionVector = Vector3.zero;
        int neighboursInFOV = 0;
        if (flockNeighbours.Count == 0)
            return cohesionVector;
        for (int i = 0; i < flockNeighbours.Count; i++)
        {
            if (IsInFOV(flockNeighbours[i].myTransform.position))
            {
                neighboursInFOV++;
                cohesionVector += flockNeighbours[i].myTransform.position;
            }
        }

        cohesionVector /= neighboursInFOV;
        cohesionVector -= myTransform.position;
        cohesionVector = cohesionVector.normalized;
        return cohesionVector;
    }

    private Vector3 CalculateAlignmentVector()
    {
        var alignmentVector = myTransform.forward;
        if (alignFlockNeighbours.Count == 0)
            return alignmentVector;
        int neighboursInFOV = 0;
        for (int i = 0; i < alignFlockNeighbours.Count; i++)
        {
            if (IsInFOV(alignFlockNeighbours[i].myTransform.position))
            {
                neighboursInFOV++;
                alignmentVector += alignFlockNeighbours[i].myTransform.forward;
            }
        }
        if (neighboursInFOV == 0)
            return myTransform.forward;
        alignmentVector /= neighboursInFOV;
        alignmentVector = alignmentVector.normalized;
        return alignmentVector;
    }

    private Vector3 CalculateAvoidanceVector()
    {
        var avoidanceVector = Vector3.zero;
        if (alignFlockNeighbours.Count == 0)
            return Vector3.zero;
        int neighboursInFOV = 0;
        for (int i = 0; i < avoidFlockNeighbours.Count; i++)
        {
            if (IsInFOV(avoidFlockNeighbours[i].myTransform.position))
            {
                neighboursInFOV++;
                avoidanceVector += (myTransform.position - avoidFlockNeighbours[i].myTransform.position);
            }
        }

        avoidanceVector /= neighboursInFOV;
        avoidanceVector = avoidanceVector.normalized;
        return avoidanceVector;
    }

    private Vector3 CalculateBoundsVector()
    {
        var centreOffset = assignedFlock.transform.position - myTransform.position;
        bool isNearCentre = (centreOffset.magnitude >= assignedFlock.boundsDistance * 0.9f);
        return isNearCentre ? centreOffset.normalized : Vector3.zero;
    }

    private Vector3 ObstacleAvoidVector()
    {
        var obstacleVector = Vector3.zero;
        RaycastHit hit;
        if (Physics.Raycast(myTransform.position, myTransform.forward, out hit, assignedFlock.obstacleDistance, obstacleMask))
        {
            obstacleVector = FindObstacleDodgeDirection();
        }
        else
        {
            currentObstacleAvoidanceVector = Vector3.zero;
        }
        return obstacleVector;
    }

    private Vector3 FindObstacleDodgeDirection()
    {
        if (currentObstacleAvoidanceVector != Vector3.zero)
        {
            RaycastHit hit;
            if (!Physics.Raycast(myTransform.position, myTransform.forward, out hit, assignedFlock.obstacleDistance, obstacleMask))
            {
                return currentObstacleAvoidanceVector;
            }
        }
        float maxDistance = int.MinValue;
        var selectedDirection = Vector3.zero;
        for (int i = 0; i < directionsObstacleDodgeCheck.Length; i++)
        {
            RaycastHit hit;
            var currentDirection = myTransform.TransformDirection(directionsObstacleDodgeCheck[i].normalized);
            if (Physics.Raycast(myTransform.position, currentDirection, out hit, assignedFlock.obstacleDistance, obstacleMask))
            {
                float currentDistance = (hit.point - myTransform.position).sqrMagnitude;
                if (currentDistance > maxDistance)
                {
                    maxDistance = currentDistance;
                    selectedDirection = currentDirection;
                }
            }
            else
            {
                selectedDirection = currentDirection;
                currentObstacleAvoidanceVector = currentDirection.normalized;
                return selectedDirection.normalized;
            }
        }
        return selectedDirection.normalized;
    }

    private bool IsInFOV(Vector3 position)
    {
        return Vector3.Angle(myTransform.forward, position - myTransform.position) <= FOVAngle;
    }

}

