using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLook : MonoBehaviour
{
    public Transform lookTarget;
    public Transform myTransform;
    public float speed = 5.0f;

    private float timeCount = 0.0f;
    private float smooth = 0.3f;
    private float distance = 5.0f;
    private float yVelocity = 0.0f;

    private Coroutine LookCoroutine;


    // Start is called before the first frame update
    void Start()
    {
        lookTarget = GameObject.FindGameObjectWithTag("Player").transform;
        myTransform = transform;

    }



    // Update is called once per frame
    void Update()
    {
        //Vector3 lookVector = (lookTarget.transform.position - myTransform.position).normalized;
        //Quaternion rotation = Quaternion.LookRotation(lookVector, Vector3.up);

        //myTransform.rotation = Quaternion.Slerp(myTransform.rotation, rotation, timeCount);
        //timeCount = timeCount + Time.deltaTime;


        StartCoroutine(LookAt());

        Vector3 moveVector = myTransform.forward;
        myTransform.position += moveVector * Time.deltaTime * speed;

    }

    

    private IEnumerator LookAt()
    {
        Quaternion lookRotation = Quaternion.LookRotation(lookTarget.position - myTransform.position);

        float time = 0;

        while (time < 1)
        {
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation, lookRotation, time);

            time += Time.deltaTime * speed;

            yield return null;
        }
    }
}
