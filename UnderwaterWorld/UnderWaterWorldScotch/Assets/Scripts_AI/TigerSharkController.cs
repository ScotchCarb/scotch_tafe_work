using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TigerSharkController : MonoBehaviour
{
    [SerializeField] private TigerSharkBehaviour tigerSharkPrefab;

    //curiosity meter variable
    [SerializeField]
    public float curiosityMeter = 0f;

    [Header("Speed Settings")]
    //tigershark speed
    [Range(0, 10)]
    [SerializeField]
    public float sharkSpeed;

    //Detection Distances:
    [Header("Detection Distances")]
    //player detection distance
    [Range(0, 10)]
    [SerializeField]
    public float playerDist;
    //cohesion (for 'flocking' with player) detection distance
    [Range(0, 10)]
    [SerializeField]
    public float cohesionDist;
    //avoidance (with player, other fish) detection distance
    [Range(0, 10)]
    [SerializeField]
    public float avoidDist;
    //bounds distance for navpoint
    [Range(0, 10)]
    [SerializeField]
    public float boundsDist;

    //Behaviour weights
    //weight for staying within navpoint bounds
    [Range(0, 10)]
    [SerializeField]
    public float boundsWeight;
    //weight for cohesion with player
    [Range(0, 10)]
    [SerializeField]
    public float cohesionWeight;
    //weight for obstacle avoidance
    [Range(0, 10)]
    [SerializeField]
    public float obstacleWeight;

    public TigerSharkBehaviour tigerSharkBehaviour { get; set; }


    // Start is called before the first frame update
    void Start()
    {
        SpawnUnit();
    }


    void Update()
    {
        //tigerSharkBehaviour.MoveUnit();
    }

    //spawn method
    //spawns a tigershark to match to the navpoint
    private void SpawnUnit()
    {
        tigerSharkBehaviour = new TigerSharkBehaviour();

        var spawnPosition = transform.position;
        var rotation = Quaternion.Euler(0, UnityEngine.Random.Range(0, 360), 0);
        Instantiate(tigerSharkPrefab, spawnPosition, rotation);
        //tigerSharkBehaviour.AssignController(this);
        

    }


}
