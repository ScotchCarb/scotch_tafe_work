using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Prototype One");
    }

    public void QuitGame()
    {
        Debug.Log("Quitted the game bye");
        Application.Quit();
    }
        
    public void GalleryMenu()
    {
        SceneManager.LoadScene("GalleryPrototype");
    }
 
}
